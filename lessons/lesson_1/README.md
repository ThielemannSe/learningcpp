# Lesson 1 - Operator Overloading #

C++ allows to provide more than just one definition for a certain function. Doing so is
called function overloading. 

### Task: ###
Extend class "Vector2D" with functions for substraction and multiplication of two vectors.